package com.mikolaj.pietkiewicz.comics.comics;

import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@RestController
public class ComicsController {
    public static List<Comics> comics = new ArrayList<>();

    @PostConstruct
    public void init() throws UnirestException {
        Unirest.setObjectMapper(UnirestConfiguration.getUnirestObjectMapper());

        int r = randomNum();
        for (int i = r; i <= r + 10; i++) {
            Comics comicses = Unirest
                    .get("https://xkcd.com/" + i + "/info.0.json")
                    .asObject(Comics.class)
                    .getBody();
            comics.add(comicses);
        }
    }

    @GetMapping("/random")
    public Comics getRandomComics() {
        Random random = new Random();
        return comics.get(random.nextInt(comics.size()));
    }

    @GetMapping("/comics")
    public List<Comics> getAllComics() {
        return comics;
    }

    @GetMapping("/comics/{id}")
    public ResponseEntity<Comics> getComics(@PathVariable String id) {
        for (Comics comics : comics) {
            if (comics.getNum().equals(id)) {
                return ResponseEntity.ok(comics);
            }
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/comics/{id}")
    public ResponseEntity<String> deleteComics(@PathVariable String id) {
        if (!comics.removeIf(c -> c.getNum().equals(id))) {
            return new ResponseEntity<>("Comic not found", HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok("Comics has been deleted");
    }

    @PostMapping("/comics")
    public ResponseEntity<String> createComics(@Valid @RequestBody Comics newComics, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return new ResponseEntity<>("Comics is null or has empty body element", HttpStatus.BAD_REQUEST);
        }
        comics.add(newComics);
        return ResponseEntity.ok("Comics has been created");
    }

    private static int randomNum() {
        Random random = new Random();
        return random.nextInt(100);
    }
}




