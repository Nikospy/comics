package com.mikolaj.pietkiewicz.comics.comics;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Comics {

    @NotNull
    @Size(min = 1)
    private String num;

    @NotNull
    @Size(min = 1)
    private String transcript;

    @NotNull
    @Size(min = 1)
    private String img;

    @NotNull
    @Size(min = 1)
    private String title;
}
