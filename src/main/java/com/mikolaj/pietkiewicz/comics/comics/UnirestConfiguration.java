package com.mikolaj.pietkiewicz.comics.comics;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UnirestConfiguration {

    @Bean
    public static UnirestObjectMapper getUnirestObjectMapper(){
        return new UnirestObjectMapper();
    }
}
